# The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.

# Find the sum of all the primes below two million.
# p = []
# N = 2_000_000
# set = (2..N).to_a

# while set.first**2 < N
#   new_set = set.map { |num| set.first * num } 
#   set -= new_set
#   p << set.shift
# end

# p += set
# puts p.inject(0) { |sum, prime| sum + prime }

p = [2]
x = 3


while x < 2_000_000
  prime_found = false

  p.each do |prime|
    if x % prime == 0
      break
    elsif prime > Math.sqrt(x).to_i
      prime_found = true
      break
    end
  end
  p << x if prime_found
  x += 2
end

puts p.inject(0) { |sum, prime| sum + prime }
