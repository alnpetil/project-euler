# The following iterative sequence is defined for the set of positive integers:

# n → n/2 (n is even)
# n → 3n + 1 (n is odd)

# Using the rule above and starting with 13, we generate the following sequence:

# 13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1
# It can be seen that this sequence (starting at 13 and finishing at 1)
# contains 10 terms. Although it has not been proved yet (Collatz Problem),
# it is thought that all starting numbers finish at 1.

# Which starting number, under one million, produces the longest chain?
longest_chain = 1
longest_starting = 1

(1..999_999).each do |num|
  chain = 1
  term = num
  loop do
    term.even? ? (term /= 2) : (term = 3 * term + 1)
    chain += 1
    break if term == 1
  end
  if chain > longest_chain 
    longest_chain = chain
    longest_starting = num
  end
end

p longest_starting
