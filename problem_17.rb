# If the numbers 1 to 5 are written out in words: one, two, three, four, five,
# then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.

# If all the numbers from 1 to 1000 (one thousand) inclusive were written out
# in words, how many letters would be used?

words = []

def single_digit(num)
  return 'one' if num == 1
  return 'two' if num == 2
  return 'three' if num == 3
  return 'four' if num == 4
  return 'five' if num == 5
  return 'six' if num == 6
  return 'seven' if num == 7
  return 'eight' if num == 8
  return 'nine' if num == 9
end

def tens(num)
  if num[0] == '1'
    return 'ten' if num[1] == '0'
    return 'eleven' if num[1] == '1'
    return 'twelve' if num[1] == '2'
    return 'thirteen' if num[1] == '3'
    return 'fourteen' if num[1] == '4'
    return 'fifteen' if num[1] == '5'
    return 'sixteen' if num[1] == '6'
    return 'seventeen' if num[1] == '7'
    return 'eighteen' if num[1] == '8'
    return 'nineteen' if num[1] == '9'
  end

  return 'twenty' if num[0] == '2' && num[1] == '0'
  return 'twenty' + single_digit(num[1].to_i) if num[0] == '2'

  return 'thirty' if num[0] == '3' && num[1] == '0'
  return 'thirty' + single_digit(num[1].to_i) if num[0] == '3'

  return 'thirty' if num[0] == '3' && num[1] == '0'
  return 'thirty' + single_digit(num[1].to_i) if num[0] == '3'

  return 'forty' if num[0] == '4' && num[1] == '0'
  return 'forty' + single_digit(num[1].to_i) if num[0] == '4'

  return 'fifty' if num[0] == '5' && num[1] == '0'
  return 'fifty' + single_digit(num[1].to_i) if num[0] == '5'

  return 'sixty' if num[0] == '6' && num[1] == '0'
  return 'sixty' + single_digit(num[1].to_i) if num[0] == '6'

  return 'seventy' if num[0] == '7' && num[1] == '0'
  return 'seventy' + single_digit(num[1].to_i) if num[0] == '7'

  return 'eighty' if num[0] == '8' && num[1] == '0'
  return 'eighty' + single_digit(num[1].to_i) if num[0] == '8'

  return 'ninety' if num[0] == '9' && num[1] == '0'
  return 'ninety' + single_digit(num[1].to_i) if num[0] == '9'
end

def hundreds(num)
  combined = ''
  return single_digit(num[0].to_i) + ' hundred' if num[1..-1] == '00'
  combined << single_digit(num[0].to_i) + ' hundred and '
  combined << (num[1] == '0' ? single_digit(num[-1].to_i) : tens(num[1..-1]))
end

(1..1000).each do |num|
  num_string = num.to_s
  words << single_digit(num) if num_string.length == 1
  words << tens(num.to_s) if num_string.length == 2
  words << hundreds(num.to_s) if num_string.length == 3
  words << 'One thousand' if num_string.length == 4
end

p words
puts
p words.join.delete(' ').length
