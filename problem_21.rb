# Let d(n) be defined as the sum of proper divisors of n (numbers less than n
# which divide evenly into n).
# If d(a) = b and d(b) = a, where a ≠ b, then a and b are an amicable pair and
# each of a and b are called amicable numbers.

# For example, the proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20, 22, 44,
# 55 and 110; therefore d(220) = 284. The proper divisors of 284 are 1, 2, 4,
# 71 and 142; so d(284) = 220.

# Evaluate the sum of all the amicable numbers under 10000.

amicable_numbers = []

def get_divisors(number)
  divisors = []
  (1..Math.sqrt(number)).each do |num|
    if (number % num).zero?
      divisors << num
      divisors << number / num unless num == 1
    end
  end
  divisors
end

def sum_of_divisors(number)
  list_of_divisors = get_divisors(number)
  list_of_divisors.inject(0) { |sum, divisor| sum + divisor }
end

# This checks if current number has an amicable pair
def amicable?(a)
  b = sum_of_divisors(a)
  return true if a == sum_of_divisors(b) && a != b
end

# Creates a list of amicable numbers under 10_000 and gets the sum.
(1..9999).each do |number|
  amicable_numbers << number if amicable?(number)
end

p amicable_numbers.inject(0) { |sum, val| sum + val }