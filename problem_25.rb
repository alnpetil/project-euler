# What is the index of the first term in the Fibonacci sequence to contain
# 1000 digits?

term = 1
next_term = 1
list = [1]

while term.to_s.length != 1000
  term, next_term = next_term, term + next_term
  list << term
end

p list.rindex(list.last) + 1
