# The prime factors of 13195 are 5, 7, 13 and 29.

# What is the largest prime factor of the number 600851475143 ?
class PrimeFactor
  attr_accessor :primes

  def initialize
    @primes = []
  end

  def get_factors(number)
    while (number % 2).zero?
      number /= 2
      @primes << 2
    end
    3.step(Math.sqrt(number).to_i, 2) do |i|
      while (number % i).zero?
        number /= i
        @primes << i
      end
    end
    @primes << number if number > 2
  end
end

p = PrimeFactor.new
p.get_factors(100)
puts p.primes
