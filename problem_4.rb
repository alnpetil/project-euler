# A palindromic number reads the same both ways. The largest palindrome made
# from the product of two 2-digit numbers is 9009 = 91 x 99.

# Find the largest palindrome made from the product of two 3-digit numbers.

product = 0
results = []

(100..999).each do |num1|
  (100..999).each do |num2|
    product = num1 * num2
    results << product if product.to_s == product.to_s.reverse
  end
end

puts results.sort.last
