# 2520 is the smallest number that can be divided by each of the numbers from 1
# to 10 without any remainder.

# What is the smallest positive number that is evenly divisible by all of the
# numbers from 1 to 20?

require './problem_3.rb'

x = 2520

loop do
  break if (2..20).all? { |i| x % i == 0 }
  x += 2520
end

puts x

y =  (2..20).to_a.map do |i|
  p = PrimeFactor.new
  p.get_factors(i)
  p.primes.inject(Hash.new(0)) do |hash, value|
    hash[value] += 1
    hash
  end
end

puts y

c = y.inject(Hash.new(0)) do |combined_hash, hash|
  hash.each do |key, val| 
    combined_hash[key] = val if combined_hash[key] < val
  end
  combined_hash
end

puts c

d = c.inject(1) do |total, (key, val)| 
  total *= key**val
  total
end

puts d
