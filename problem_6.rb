

sum_of_squares = (1..100).inject(0) do |sum, value|
  sum + value**2
end

sum_of_num = (1..100).inject(0) do |sum, value|
  sum + value
end

puts (sum_of_num**2) - sum_of_squares
