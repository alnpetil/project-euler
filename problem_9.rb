# There exists exactly one Pythagorean triplet for which a + b + c = 1000.
# Find the product abc.

# a < b < c
finished = false

(1..1000).each do |a|
  (1..1000 - a).each do |b|
    c = 1000 - a - b
    if (a**2 + b**2) == c**2
      finished = true
      puts "a = #{a}, b = #{b}, c = #{c}"
      puts "Product is #{a * b * c}."
      break
    end
    break if finished
  end
  break if finished
end
